<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $fillable = ['id' , 'name'];

    protected $primaryKey = 'id';

    protected $keyType = 'int';

    public $incrementing = false;

    public function Users()
    {
        return $this->hasMany('App\Users');
    }
}
